Welcome to Stansbury Manor ! Come join our community and surround yourself with the warm friendly atmosphere of home. We have a variety of attractive one and two bedroom apartment floor plans and two and three bedroom townhomes to fit your every need.

Address: 1 Alder Drive, #A, Baltimore, MD 21220, USA

Phone: 410-391-5151

Website: https://www.stansburymanor.com
